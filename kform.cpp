#include "kform.h"
#include "ui_kform.h"
#include <QDebug>
KForm::KForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::KForm)
{
    ui->setupUi(this);                          //Set Scene for graphicsView
    Graphic = new KGraphic();
    ui->graphicsView->setScene(Graphic);
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());    //Thêm 1 hàng trống
    List = new KList();
}

KForm::~KForm()
{
    delete List;
    delete Graphic;
    delete ui;
}

void KForm::on_tableWidget_cellChanged(int row, int column)
{
    if(column > 0 && column <=3)  // kiểm tra dữ liệu của dòng mới thêm vào
    {
        bool ok;
        int i;
        i = ui->tableWidget->item(row,column)->text().toInt(&ok,10);
        if(!ok || (column == 2 && i == 0))
        {
            ui->tableWidget->item(row,column)->setText("");
            return;
        }
    }
    if(row == ui->tableWidget->rowCount() - 1)
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
}
void KForm::keyPressEvent(QKeyEvent * event)   // Xóa Hàng được chọn
{
    if(event->key() == Qt::Key_Delete && ui->tableWidget->rowCount()> 1) // Xóa hàng khi nhấn delete
        ui->tableWidget->removeRow(ui->tableWidget->currentRow());
}
void KForm::on_B_FIFO_clicked()
{
    if(ui->tableWidget->rowCount() == 1)
        return;
    List->AddListProcess(ui->tableWidget);
    List->FIFO(ui->checkBox->isChecked());

    ui->horizontalSlider->setMaximum(Graphic->Action(List));

    Tinh_T_Cho();
}

void KForm::on_B_STF_clicked()
{
    if(ui->tableWidget->rowCount() == 1)
        return;
    List->AddListProcess(ui->tableWidget);
    List->STF(ui->checkBox->isChecked());
    ui->horizontalSlider->setMaximum(Graphic->Action(List));

    Tinh_T_Cho();
}

void KForm::on_B_Priority_clicked()
{
    if(ui->tableWidget->rowCount() == 1)
        return;
    List->AddListProcess(ui->tableWidget,1);
    List->Priority(ui->checkBox->isChecked());
    ui->horizontalSlider->setMaximum(Graphic->Action(List));

    Tinh_T_Cho();
}

void KForm::on_B_RR_clicked()
{
    if(ui->tableWidget->rowCount() == 1)
        return;
    List->AddListProcess(ui->tableWidget);
    List->RR(ui->checkBox->isChecked(),ui->spinBox->text().toInt());
    ui->horizontalSlider->setMaximum(Graphic->Action(List));

    Tinh_T_Cho();
}

void KForm::on_B_SRTF_clicked()
{
    if(ui->tableWidget->rowCount() == 1)
        return;
    List->AddListProcess(ui->tableWidget);
    List->SRTF(ui->checkBox->isChecked());
    ui->horizontalSlider->setMaximum(Graphic->Action(List));

    Tinh_T_Cho();
}

void KForm::on_horizontalSlider_sliderMoved(int position)
{
    ui->graphicsView->scene()->setSceneRect(position + 200,0,1,1);
}

void KForm::on_label_linkActivated(const QString &link)
{
}

void KForm::on_pushButton_clicked()
{
    QDesktopServices::openUrl(QUrl("https://www.facebook.com/Hung.Hanhcu", QUrl::TolerantMode));
}

void KForm::Tinh_T_Cho()
{
    for (int i = 0; i< List->ListRun->count(); i++) {

        if(List->ListRun->at(i).ID >-1){
            if(ui->tableWidget->item(List->ListRun->at(i).ID,4) == NULL)
                ui->tableWidget->setItem(List->ListRun->at(i).ID,4, new QTableWidgetItem());
            ui->tableWidget->item(List->ListRun->at(i).ID,4)->setText(QString::number(*List->ListRun->at(i).T_Cho));
        }
    }
    ui->T_Trung_Binh->setText("Thời gian chờ trung bình: " + QString::number(List->TrungBinh()));
}

