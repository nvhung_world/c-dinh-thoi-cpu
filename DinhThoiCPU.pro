#-------------------------------------------------
#
# Project created by QtCreator 2016-10-28T01:13:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DinhThoiCPU
TEMPLATE = app


SOURCES += main.cpp \
    kform.cpp \
    kgraphic.cpp \
    kitem.cpp \
    klist.cpp \
    kprocess.cpp

HEADERS  += \
    kform.h \
    kgraphic.h \
    kitem.h \
    klist.h \
    kprocess.h

FORMS    += \
    kform.ui

RESOURCES += \
    anh.qrc

DISTFILES += \
    icon.ico
