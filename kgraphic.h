#ifndef KGRAPHIC_H
#define KGRAPHIC_H

#include <QGraphicsscene>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QPen>
#include <QLine>
#include "klist.h"
#include "kprocess.h"
#include "kitem.h"
class KGraphic  : public QGraphicsScene
{
public:
    KGraphic();
    ~KGraphic();
    int Action(KList *List);
    int HeSo, Leng;
};

#endif // KGRAPHIC_H
