#include "kitem.h"
KItem::KItem(KProcess *process, int Heso) : QGraphicsItem(NULL)
{
    name  = *process->name;
    T_Vao = *process->T_Vao * Heso;
    T_Thuc_Thi = *process->T_Thuc_Thi * Heso;
    heSo = Heso;
    color = *process->Color;
}

KItem::~KItem()
{

}
QRectF KItem::boundingRect() const
{
    return QRectF(T_Vao,20,T_Thuc_Thi,10);
}

void KItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::black);       // Set mầu

    // Ve Thoi gian vào
    painter->drawLine(T_Vao,25,T_Vao,15);
    painter->drawText(T_Vao - 5,35,QString::number(T_Vao/heSo));
    //Ve thoi gian thục thi
    painter->drawLine(T_Thuc_Thi + T_Vao,25,T_Thuc_Thi + T_Vao,15);
    painter->drawText(T_Vao + T_Thuc_Thi / 2,-3,QString::number(T_Thuc_Thi/heSo));

    QBrush *B = new QBrush(color);      // Set mầu
    painter->setBrush(*B);
    // Ve hình chữ nhật
    painter->drawRoundedRect(T_Vao,0,T_Thuc_Thi,20,10,10);
    painter->drawText(T_Vao + T_Thuc_Thi / 2,12.5f,name);
}
