#ifndef KITEM_H
#define KITEM_H

#include "kprocess.h"
#include <QString>
#include <QGraphicsitem>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include <QRectF>
#include <QColor>
#include <QBrush>
class KItem : public QGraphicsItem
{
public:
    KItem(KProcess *process,int Heso);
    ~KItem();
    QRectF boundingRect() const;
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
protected:
    QString name;
    int T_Vao, T_Thuc_Thi, heSo, index;
    QColor color;
};

#endif // KITEM_H
