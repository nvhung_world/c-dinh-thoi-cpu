#ifndef KFORM_H
#define KFORM_H

#include <QWidget>
#include <QKeyEvent>
#include <QGraphicsscene>
#include <QLineEdit>
#include <QDesktopServices>
#include <QTableWidgetItem>
#include "klist.h"
#include "kgraphic.h"
#include "kprocess.h"
namespace Ui {
class KForm;
}

class KForm : public QWidget
{
    Q_OBJECT

public:
    explicit KForm(QWidget *parent = 0);
    ~KForm();
    KList *List;
protected:
    void keyPressEvent(QKeyEvent *event);
private slots:
    void on_tableWidget_cellChanged(int row, int column);

    void on_B_FIFO_clicked();

    void on_B_STF_clicked();

    void on_B_Priority_clicked();

    void on_B_RR_clicked();

    void on_B_SRTF_clicked();

    void on_horizontalSlider_sliderMoved(int position);

    void on_label_linkActivated(const QString &link);

    void on_pushButton_clicked();

private:
    void Tinh_T_Cho();
    Ui::KForm * ui;
    KGraphic * Graphic;
};

#endif // KFORM_H
