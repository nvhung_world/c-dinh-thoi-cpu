#include "kprocess.h"
KProcess::KProcess()
{
    name = new QString();
    ID = -1;
    T_Vao = new int(0);
    T_Thuc_Thi = new int(0);
    Do_Uu_Tien = new int(0);
    T_Da_Thuc_Thi = new int(0);
    T_Hoan_Thanh = new int(0);
    T_Cho = new int(0);
    Color = new QColor(qrand()%255,qrand()%255,qrand() % 255);
}
KProcess::~KProcess()
{
    if(name != 0)
        delete name;
    //if(ID != 0)
        //delete ID;
    if(T_Vao != 0)
        delete T_Vao;
    if(T_Thuc_Thi != 0)
        delete T_Thuc_Thi;
    if(Do_Uu_Tien != 0)
        delete Do_Uu_Tien;
    if(T_Cho != 0)
        delete T_Cho;
    if(T_Hoan_Thanh != 0)
        delete T_Hoan_Thanh;
    if(T_Da_Thuc_Thi !=0)
        delete T_Da_Thuc_Thi;
    if(Color !=0)
        delete Color;
}
