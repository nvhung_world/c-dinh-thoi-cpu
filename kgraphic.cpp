#include "kgraphic.h"
#include <QString>
KGraphic::KGraphic() : QGraphicsScene()
{
    this->setSceneRect(200,0,1,1);
    this->addLine(-10,20,200,20,*( new QPen(Qt::black)));
    this->addLine(0,25,0,-30,*( new QPen(Qt::black)));
    this->addText("0")->setPos(-10,20);
    HeSo = 20;
    Leng = 0;
}

KGraphic::~KGraphic()
{
}
int KGraphic::Action(KList *List)
{
    this->clear();
    for(int i = 0; i< List->ListRun->count();i++){
        KProcess *P = new KProcess();
        *P->name = *List->ListRun->at(i).name;
        *P->T_Vao = *List->ListRun->at(i).T_Vao;
        *P->T_Thuc_Thi = *List->ListRun->at(i).T_Thuc_Thi;
        if(!i && *P->T_Vao > 0)
            this->addText("0")->setPos(-10,20);
        if(i < List->ListRun->count() - 1)
        {
            if(*P->T_Vao + *P->T_Thuc_Thi < *List->ListRun->at(i + 1).T_Vao)
            {
                this->addLine((*P->T_Vao + *P->T_Thuc_Thi) * HeSo,25,(*P->T_Vao + *P->T_Thuc_Thi) * HeSo,15,*( new QPen(Qt::black)));
                this->addText(QString::number(*P->T_Vao + *P->T_Thuc_Thi))->setPos((*P->T_Vao + *P->T_Thuc_Thi) * HeSo -10,20);
            }
        }
        else
        {
                this->addLine((*P->T_Vao + *P->T_Thuc_Thi) * HeSo,25,(*P->T_Vao + *P->T_Thuc_Thi) * HeSo,15,*( new QPen(Qt::black)));
                this->addText(QString::number(*P->T_Vao + *P->T_Thuc_Thi))->setPos((*P->T_Vao + *P->T_Thuc_Thi) * HeSo -10,20);
        }
        *P->Color = *List->ListRun->at(i).Color;
        KItem *kitem = new KItem(P,HeSo);
        this->addItem(kitem);
        if(i == List->ListRun->count() - 1)
            Leng = *P->T_Thuc_Thi + *P->T_Vao;
    }
    this->addLine(-10,20,Leng * HeSo + 100,20,*( new QPen(Qt::black)));
    this->addLine(0,25,0,-30,*( new QPen(Qt::black)));
    update();
    return Leng*HeSo;
}
