#include "klist.h"
KList::KList()
{
    ListProcess = new QList<KProcess>();
    ListRun = new QList<KProcess>();
}
KList::~KList()
{
    delete ListProcess;
    delete ListRun;
}

void KList::AddListProcess(QTableWidget *table, int Do_Uu_Tien)     // lạp dữ liệu vào hàng chờ CPU
{
    ListProcess->clear();
    for(int i = 0; i<table->rowCount() - 1;i++)
    {
        KProcess *P = new KProcess();
        if(table->item(i,0) != 0)
            *P->name = table->item(i,0)->text();
        else
            *P->name = "";

        if(table->item(i,1) != 0)
            *P->T_Vao = table->item(i,1)->text().toInt();

        if(table->item(i,2) != 0)
            *P->T_Thuc_Thi = table->item(i,2)->text().toInt();
        else
            continue;

        if(Do_Uu_Tien)
        {
            if(table->item(i,3) != 0)
                *P->Do_Uu_Tien = table->item(i,3)->text().toInt();
        }

        if(*P->T_Thuc_Thi > 0)
        {
            P->ID = i;
            *ListProcess<<*P;
        }
    }
    Count_Process = ListProcess->count();
}
void KList::XapXepListProcess()     //xắp xếp danh sách hành chờ CPU theo thứ tự thời gian vào CPU tăng dần
{
    for(int i = 0; i < ListProcess->count();i++)
        for(int j = 0; j < ListProcess->count() - 1;j++)
            if(*ListProcess->at(j).T_Vao > *ListProcess->at(j+1).T_Vao)
                ListProcess->move(j,j+1);
}
void KList::FIFO(int T_Vao)
{
    ListRun->clear();
    if(T_Vao)
        XapXepListProcess();
    *ListRun<<*ListProcess;
    int T = 0;
    for(int i =0; i<ListRun->count();i++){
        if(!T_Vao)
            *ListRun->at(i).T_Vao = 0;
        if(*ListRun->at(i).T_Vao <= T){
            *ListRun->at(i).T_Cho = T - *ListRun->at(i).T_Vao;
            *ListRun->at(i).T_Vao = T;
        }
        T = *ListRun->at(i).T_Vao + *ListRun->at(i).T_Thuc_Thi;
        *ListRun->at(i).T_Hoan_Thanh = T;
    }
}

void KList::STF(int T_Vao)
{
    ListRun->clear();
    if(T_Vao)
        XapXepListProcess();
    int T = 0;
    while (ListProcess->count()){
        int index = 0;
        for(int i = 0; i<ListProcess->count(); i++){
            if(!T_Vao)
                *ListProcess->at(i).T_Vao = 0;
            if( *ListProcess->at(i).T_Vao > T)
                break;
            else if(*ListProcess->at(i).T_Thuc_Thi < *ListProcess->at(index).T_Thuc_Thi)
                index = i;
        }
        KProcess *P = new KProcess();
        *P->name = *ListProcess->at(index).name;
        *P->T_Thuc_Thi = *ListProcess->at(index).T_Thuc_Thi;
        if(*ListProcess->at(index).T_Vao < T){
            *P->T_Vao = T;
            *P->T_Cho = T - *ListProcess->at(index).T_Vao;
        }
        else{
            *P->T_Vao = *ListProcess->at(index).T_Vao;
            *P->T_Cho = 0;
        }
        T = *P->T_Hoan_Thanh = *P->T_Vao + *P->T_Thuc_Thi;
        P->ID = ListProcess->at(index).ID;
        *ListRun<<*P;
        ListProcess->removeAt(index);
    }
}

void KList::Priority(int T_Vao)
{
    ListRun->clear();
    if(T_Vao)
        XapXepListProcess();
    int T = 0;
    while (ListProcess->count()){
        int index = 0;
        for(int i = 0; i<ListProcess->count(); i++){
            if(!T_Vao)
                *ListProcess->at(i).T_Vao = 0;
            if(*ListProcess->at(i).T_Vao > T)
                break;
            else if(*ListProcess->at(i).Do_Uu_Tien < *ListProcess->at(index).Do_Uu_Tien)
                index = i;
        }
        KProcess *P = new KProcess();
        *P->name = *ListProcess->at(index).name;
        *P->T_Thuc_Thi = *ListProcess->at(index).T_Thuc_Thi;
        *P->Do_Uu_Tien = *ListProcess->at(index).Do_Uu_Tien;
        if(*ListProcess->at(index).T_Vao < T){
            *P->T_Vao = T;
            *P->T_Cho = T - *ListProcess->at(index).T_Vao;
        }
        else{
            *P->T_Vao = *ListProcess->at(index).T_Vao;
            *P->T_Cho = 0;
        }
        T = *P->T_Hoan_Thanh = *P->T_Vao + *P->T_Thuc_Thi;
        P->ID = ListProcess->at(index).ID;
        *ListRun<<*P;
        ListProcess->removeAt(index);
    }
}

void KList::RR(int T_Vao, int quantum)
{
    ListRun->clear();
    if(T_Vao)
        XapXepListProcess();
    int index = 0,
        T = 0;
    while(ListProcess->count()){
        if(!T_Vao)
            *ListProcess->at(index).T_Vao = 0;
        else if(!index && T < *ListProcess->at(index).T_Vao)
            T = *ListProcess->at(index).T_Vao;

        KProcess *P = new KProcess();
        *P->name = *ListProcess->at(index).name;
        *P->Color = *ListProcess->at(index).Color;
        *P->T_Vao = T;

        int Thuc_Thi = 0;
        if(*ListProcess->at(index).T_Thuc_Thi > quantum )
            Thuc_Thi = quantum;
        else{
             Thuc_Thi = *ListProcess->at(index).T_Thuc_Thi;
             *P->T_Hoan_Thanh = T + Thuc_Thi;
             *P->T_Cho = T - *ListProcess->at(index).T_Da_Thuc_Thi - (T_Vao? *ListProcess->at(index).T_Vao:0);
             P->ID = ListProcess->at(index).ID;
        }
        *P->T_Thuc_Thi = Thuc_Thi;
        T = *P->T_Vao + *P->T_Thuc_Thi;
        *ListRun<<*P;

        *ListProcess->at(index).T_Da_Thuc_Thi += Thuc_Thi;
        *ListProcess->at(index).T_Thuc_Thi -= Thuc_Thi;
        if(*ListProcess->at(index).T_Thuc_Thi == 0){
            ListProcess->removeAt(index);
            index --;
        }

        if(index < ListProcess->count() - 1 && ( !T_Vao || *ListProcess->at(index +1).T_Vao <= T)){
            index ++;
        }
        else
            index = 0;
    }
}

void KList::SRTF(int T_Vao)
{
    ListRun->clear();
    if(T_Vao)
        XapXepListProcess();
    int T = 0;
    while (ListProcess->count()){
        int index = 0,
            leng = *ListProcess->at(index).T_Thuc_Thi;
        if(!index && T_Vao && *ListProcess->at(index).T_Vao > T)
            T = *ListProcess->at(index).T_Vao;
        for(int i = 0; i<ListProcess->count();i++){
            if(T_Vao && *ListProcess->at(i).T_Vao > T){
                if(*ListProcess->at(i).T_Thuc_Thi < *ListProcess->at(index).T_Thuc_Thi - (*ListProcess->at(i).T_Vao - T)){
                    if(*ListProcess->at(index).T_Thuc_Thi < (*ListProcess->at(i).T_Vao - T))
                        leng = *ListProcess->at(index).T_Thuc_Thi;
                    else
                        leng = (*ListProcess->at(i).T_Vao - T);
                }
            }
            else {
                if(*ListProcess->at(i).T_Thuc_Thi < *ListProcess->at(index).T_Thuc_Thi)
                {
                    leng = *ListProcess->at(i).T_Thuc_Thi;
                    index = i;
                }
            }
        }

        KProcess *P = new KProcess();
        *P->name = *ListProcess->at(index).name;
        *P->Color = *ListProcess->at(index).Color;
        *P->T_Vao = T;
        *P->T_Thuc_Thi = leng;

        *ListProcess->at(index).T_Da_Thuc_Thi += leng;
        *ListProcess->at(index).T_Thuc_Thi -= leng;
        if(*ListProcess->at(index).T_Thuc_Thi == 0){
            *P->T_Hoan_Thanh = T + leng;
            *P->T_Cho = *P->T_Hoan_Thanh - ((T_Vao? *ListProcess->at(index).T_Vao:0) + *ListProcess->at(index).T_Da_Thuc_Thi);
            P->ID = ListProcess->at(index).ID;
            ListProcess->removeAt(index);
        }
        *ListRun<<*P;
        T+=leng;
    }
    //for(int i = 0; i < ListRun->count(); i++)
        //qDebug()<<*ListRun->at(i).name<<":"<<*ListRun->at(i).T_Vao<<":"<<*ListRun->at(i).T_Thuc_Thi<<":"<<*ListRun->at(i).Do_Uu_Tien;

}

float KList::TrungBinh()
{
    if(!Count_Process)
        return 0;
    float F = 0;
    for(int i = 0;i<ListRun->count();i++)
        F+= *ListRun->at(i).T_Cho;
    return F/ Count_Process;
}
