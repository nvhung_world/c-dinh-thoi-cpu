#ifndef KLIST_H
#define KLIST_H

#include <QList>
#include "kprocess.h"
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QString>
class KList
{
public:
    KList();
    ~KList();
    QList<KProcess> * ListProcess,
                    * ListRun;
    int Time_Max,
        Count_Process;
    void AddListProcess(QTableWidget *table, int Do_Uu_Tien = 0);
    void FIFO(int T_Vao = 0);
    void STF(int T_Vao = 0);
    void Priority(int T_Vao = 0);
    void RR(int T_Vao = 0, int quantum = 5);
    void SRTF(int T_Vao = 0);
    float TrungBinh();
protected:
    void XapXepListProcess();
};

#endif // KLIST_H
