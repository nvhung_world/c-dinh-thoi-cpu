#ifndef KPROCESS_H
#define KPROCESS_H
#include <QString>
#include <QColor>
class KProcess
{
public:
    KProcess();
    ~KProcess();
    QString *name;
    int ID, *T_Vao, *T_Thuc_Thi, *Do_Uu_Tien, *T_Cho, *T_Hoan_Thanh, *T_Da_Thuc_Thi;
    QColor *Color;
};

#endif // KPROCESS_H
